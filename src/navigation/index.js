import {
  StackNavigator,
} from 'react-navigation';

import Home from '../screen/home/index.js';
import Profile from '../screen/profile/index.js';



export default App = StackNavigator({
  Home: { screen: Home },
  Profile: { screen: Profile },
});